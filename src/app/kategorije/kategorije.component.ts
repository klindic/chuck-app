import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-kategorije',
  templateUrl: './kategorije.component.html',
  styleUrls: ['./kategorije.component.css']
})
export class KategorijeComponent {

  private categoriesApi = 'https://api.chucknorris.io/jokes/categories';
  private categoryApi = 'https://api.chucknorris.io/jokes/random?category=';
  private categories = [];
  private allJokes = [];

  constructor( private http: Http ) { 
    this.getCategories();
    this.getData();
  }

  getData() {
    return this.http.get(this.categoriesApi)
      .map((res: Response) => res.json());
  }

  getCategories() {
    this.getData().subscribe(categories => {
      this.categories = categories;
    })
  }

  getCategoryJoke(category: String) {
    this.http.get(this.categoryApi + category)
      .map((res: Response) => res.json())
        .subscribe(joke => {
        this.allJokes.unshift(joke);
    })
  }

  deleteJoke(joke) {
    for (let i = 0; i < this.allJokes.length; i++) {
      if (this.allJokes[i] == joke) {
        this.allJokes.splice(i,1);
      } 
    }
  }
}
