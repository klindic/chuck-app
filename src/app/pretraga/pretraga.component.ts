import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';
import { SearchService } from 'app/search.service';
import { empty } from 'rxjs/Observer';

@Component({
  selector: 'app-pretraga',
  templateUrl: './pretraga.component.html',
  styleUrls: ['./pretraga.component.css'],
  providers: [SearchService]
})
export class PretragaComponent {

  private allJokes = [];
  private searchKey = new Subject<string>();

  constructor( private http: Http,
              private searchService: SearchService ) {
                if(!this.searchKey.thrownError)
                {
      this.searchService.search(this.searchKey)
        .subscribe(results => {
          console.log(results.result);
          this.allJokes = results.result;
      });
    }
  }
  
  deleteJoke(joke) {
    for (let i = 0; i < this.allJokes.length; i++) {
      if (this.allJokes[i] == joke) {
        this.allJokes.splice(i,1);
      } 
    }
  }
}
