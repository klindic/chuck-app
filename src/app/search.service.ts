import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class SearchService {
  searchApi: string = 'https://api.chucknorris.io/jokes/search?query=';

  constructor(private http: Http) { }

  search(terms: Observable<string>) {
    return terms.debounceTime(50)
      .distinctUntilChanged()
        .switchMap(term => this.searchEntries(term));
  }

  searchEntries(term) {
    if (!term) {
      return this.http
        .get(this.searchApi + '{query}')
          .map(res => res.json());
    }
    return this.http
      .get(this.searchApi + term)
        .map(res => res.json());
  }
}