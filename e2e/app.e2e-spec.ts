import { ChuckAppPage } from './app.po';

describe('chuck-app App', () => {
  let page: ChuckAppPage;

  beforeEach(() => {
    page = new ChuckAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
